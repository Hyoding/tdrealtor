package com.example.josep.tdrealtor;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Pair;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.example.josep.tdrealtor.property.Property;
import com.example.josep.tdrealtor.property.PropertyUtility;
import com.example.josep.tdrealtor.session.MySession;
import com.example.josep.tdrealtor.task.RetrieveRealtorDataAsync;
import com.example.josep.tdrealtor.utility.DemoUtils;
import com.google.ar.core.ArCoreApk;
import com.google.ar.core.Frame;
import com.google.ar.core.Session;
import com.google.ar.core.TrackingState;
import com.google.ar.core.exceptions.CameraNotAvailableException;
import com.google.ar.core.exceptions.UnavailableException;
import com.google.ar.sceneform.ArSceneView;
import com.google.ar.sceneform.Node;
import com.google.ar.sceneform.rendering.ViewRenderable;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import uk.co.appoly.arcorelocation.LocationMarker;
import uk.co.appoly.arcorelocation.LocationScene;
import uk.co.appoly.arcorelocation.rendering.LocationNode;
import uk.co.appoly.arcorelocation.rendering.LocationNodeRender;
import uk.co.appoly.arcorelocation.utils.ARLocationPermissionHelper;

import static com.example.josep.tdrealtor.property.PropertyUtility.getPropertiesForAR;

public class ARActivity extends AppCompatActivity {

    private boolean installRequested;
    private boolean hasFinishedLoading = false;

    private ArSceneView arSceneView;

    List<Pair<Property, ViewRenderable>> arContainer;
    // Our ARCore-Location scene
    private LocationScene locationScene;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        checkAvailability();

        setContentView(R.layout.activity_ar);

        arContainer = new ArrayList<>();
        arSceneView = findViewById(R.id.ar_scene_view);

        new RetrieveRealtorDataAsync(() -> {
            initRenderables();
            updateAR();
        }).execute(MySession.getInstance().getCoordinate());


        // Lastly request CAMERA & fine location permission which is required by ARCore-Location.
        ARLocationPermissionHelper.requestPermission(this);
    }

    private void checkAvailability() {
        ArCoreApk.Availability availability = ArCoreApk.getInstance().checkAvailability(this);
        if (availability == ArCoreApk.Availability.UNSUPPORTED_DEVICE_NOT_CAPABLE || availability == ArCoreApk.Availability.SUPPORTED_NOT_INSTALLED) {
            Toast.makeText(this, "Device not supported", Toast.LENGTH_LONG).show();
            finish();
        }
    }

    private void initRenderables() {
        List<Property> propertyList = getPropertiesForAR(3, MySession.getInstance().getProperties(), MySession.getInstance().getCoordinate());
//        List<Property> propertyList = MySession.getInstance().getProperties();
        List<CompletableFuture<ViewRenderable>> futureVRs = new ArrayList<>();
        propertyList.forEach( property -> {
            CompletableFuture<ViewRenderable> renderable = ViewRenderable.builder()
                .setView(this, R.layout.example_layout)
                .build();

            futureVRs.add(renderable);
        });

        CompletableFuture.allOf(futureVRs.toArray(new CompletableFuture[futureVRs.size()]))
            .handle(
                (notUsed, throwable) -> {
                    if (throwable != null) {
                        DemoUtils.displayError(this, "Unable to load renderables", throwable);
                        return null;
                    }

                    int size = propertyList.size() >= 10 ? 10 : propertyList.size(); // TODO: somehow optimize this? for now, its only getting 10

                    try {
                        for (int i = 0; i < size; ++i) {
                            arContainer.add(new Pair(propertyList.get(i), futureVRs.get(i).get()));
                        }
                        hasFinishedLoading = true;

                    } catch (InterruptedException | ExecutionException ex) {
                        DemoUtils.displayError(this, "Unable to load renderables", ex);
                    }

                    return null;
                });
    }

    private void updateAR() {
        arSceneView
            .getScene()
            .setOnUpdateListener(
                    frameTime -> {
                        if (!hasFinishedLoading) {
                            return;
                        }

                        if (locationScene == null) {
                            // If our locationScene object hasn't been setup yet, this is a good time to do it
                            // We know that here, the AR components have been initiated.
                            locationScene = new LocationScene(this, this, arSceneView);

                            for (int i = 0; i < arContainer.size(); ++i) {
                                int index = i;
                                LocationMarker layoutLocationMarker = new LocationMarker(
                                    arContainer.get(i).first.getCoordinate().getlongitude(),
                                    arContainer.get(i).first.getCoordinate().getLatitude(),
                                    getExampleView(arContainer.get(i).second)
                                );

                                layoutLocationMarker.setRenderEvent(new LocationNodeRender() {
                                    @Override
                                    public void render(LocationNode node) {
                                        View eView = arContainer.get(index).second.getView();
                                        TextView distanceTextView = eView.findViewById(R.id.textView);
                                        distanceTextView.setText(node.getDistance() + "M" + "\n" + PropertyUtility.showMoney(arContainer.get(index).first.getPrice()));
                                    }
                                });

                                // Adding the marker
                                locationScene.mLocationMarkers.add(layoutLocationMarker);
                            };
                        }

                        Frame frame = arSceneView.getArFrame();
                        if (frame == null) {
                            return;
                        }

                        if (frame.getCamera().getTrackingState() != TrackingState.TRACKING) {
                            return;
                        }

                        if (locationScene != null) {
                            locationScene.processFrame(frame);
                        }
                    });
    }

    /**
     * Example node of a layout
     *
     * @return
     */
    int counter = 0;
    private Node getExampleView(ViewRenderable renderable) {
        Node base = new Node();
        if (counter == 0) {
            renderable.setVerticalAlignment(ViewRenderable.VerticalAlignment.TOP);
        } else if(counter == 1) {
            renderable.setVerticalAlignment(ViewRenderable.VerticalAlignment.CENTER);
        } else {
            renderable.setVerticalAlignment(ViewRenderable.VerticalAlignment.BOTTOM);
            counter = 0;
        }
        ++counter;

        base.setRenderable(renderable);
        Context c = this;
        // Add  listeners etc here
        View eView = renderable.getView();
        eView.setOnTouchListener((v, event) -> {
            // find property related to renderable
            Property property = null;
            for (int i = 0; i < arContainer.size(); ++i) {
                if (arContainer.get(i).second.equals(renderable)) {
                    property = arContainer.get(i).first;
                    break;
                }
            }

            Intent intet = new Intent();

            Intent intent = new Intent(this, PropertyActivity.class);
            intent.putExtra(Property.class.getSimpleName(), property);
            startActivity(intent);

            return false;
        });

        return base;
    }

    /***
     * Example Node of a 3D model
     *
     * @return

    /**
     * Make sure we call locationScene.resume();
     */
    @Override
    protected void onResume() {
        super.onResume();

        if (locationScene != null) {
            locationScene.resume();
        }

        if (arSceneView.getSession() == null) {
            // If the session wasn't created yet, don't resume rendering.
            // This can happen if ARCore needs to be updated or permissions are not granted yet.
            try {
                Session session = DemoUtils.createArSession(this, installRequested);
                if (session == null) {
                    installRequested = ARLocationPermissionHelper.hasPermission(this);
                    return;
                } else {
                    arSceneView.setupSession(session);
                }
            } catch (UnavailableException e) {
                DemoUtils.handleSessionException(this, e);
            }
        }

        try {
            arSceneView.resume();
        } catch (CameraNotAvailableException ex) {
            DemoUtils.displayError(this, "Unable to get camera", ex);
            finish();
            return;
        }
    }

    /**
     * Make sure we call locationScene.pause();
     */
    @Override
    public void onPause() {
        super.onPause();

        if (locationScene != null) {
            locationScene.pause();
        }

        arSceneView.pause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        arSceneView.destroy();
    }

    @Override
    public void onRequestPermissionsResult(
            int requestCode, @NonNull String[] permissions, @NonNull int[] results) {
        if (!ARLocationPermissionHelper.hasPermission(this)) {
            if (!ARLocationPermissionHelper.shouldShowRequestPermissionRationale(this)) {
                // Permission denied with checking "Do not ask again".
                ARLocationPermissionHelper.launchPermissionSettings(this);
            } else {
                Toast.makeText(
                        this, "Camera permission is needed to run this application", Toast.LENGTH_LONG)
                        .show();
            }
            finish();
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            // Standard Android full-screen functionality.
            getWindow()
                    .getDecorView()
                    .setSystemUiVisibility(
                            View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }
    }
}
