package com.example.josep.tdrealtor.task;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.widget.ImageView;

import java.io.InputStream;
import java.net.URL;

public class RetrieveAndDisplayImageAsync extends AsyncTask<String, Void, Drawable> {
    private Exception exception;
    ImageView imageView;

    public RetrieveAndDisplayImageAsync(ImageView imageView) {
        this.imageView = imageView;
    }

    protected Drawable doInBackground(String... urls) {
        try {
            InputStream is = (InputStream) new URL(urls[0]).getContent();
            Drawable d = Drawable.createFromStream(is, "src name");
            is.close();
            return d;
        } catch (Exception e) {
            this.exception = e;
            return null;
        }
    }

    public void onPostExecute(Drawable image) {
//        int width = Resources.getSystem().getDisplayMetrics().widthPixels;
//        imageView.setImageBitmap(Bitmap.createScaledBitmap(((BitmapDrawable)image).getBitmap(), width, 200, true));
        if (image == null) return;
        imageView.setImageDrawable(image);
    }
}