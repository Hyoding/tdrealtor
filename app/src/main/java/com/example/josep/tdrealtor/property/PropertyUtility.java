package com.example.josep.tdrealtor.property;

import com.example.josep.tdrealtor.location.Coordinate;
import com.example.josep.tdrealtor.location.CoordinateUtility;
import com.example.josep.tdrealtor.property.Property;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

public class PropertyUtility {
    public static List<Property> sortByDistanceAsc(List<Property> properties, Coordinate loc) {
        Collections.sort(properties, (a, b) -> CoordinateUtility.distanceInKM(a.getCoordinate(), loc) < CoordinateUtility.distanceInKM(b.getCoordinate(), loc) ? -1
                : CoordinateUtility.distanceInKM(a.getCoordinate(), loc) == CoordinateUtility.distanceInKM(b.getCoordinate(), loc) ? 0
                : 1);
        return properties;
    }

    public static List<Property> sortByDistanceDesc(List<Property> properties, Coordinate loc) {
        Collections.sort(properties, (a, b) -> CoordinateUtility.distanceInKM(a.getCoordinate(), loc) > CoordinateUtility.distanceInKM(b.getCoordinate(), loc) ? -1
                : CoordinateUtility.distanceInKM(a.getCoordinate(), loc) == CoordinateUtility.distanceInKM(b.getCoordinate(), loc) ? 0
                : 1);
        return properties;
    }

    public static List<Property> sortByPriceAsc(List<Property> properties) {
        Collections.sort(properties, Comparator.comparingInt(Property::getPrice));
        return properties;
    }

    public static List<Property> sortByPriceDesc(List<Property> properties) {
        Collections.sort(properties, (a ,b) -> b.getPrice() - a.getPrice());
        return properties;
    }

    public static List<Property> getPropertiesForAR(int numProperty, List<Property> properties, Coordinate myLoc) {
        List<Property> propertiesForAR = new ArrayList<>();
        List<List<Property>> classifiedProperties = classfyByDirection(properties, myLoc);
        for(List<Property> directionProperties : classifiedProperties) {
            int num = numProperty < directionProperties.size() ? numProperty : directionProperties.size();
            for(int i =0; i < num; ++i) {
                propertiesForAR.add(directionProperties.get(i));
            }
        }
        return  propertiesForAR;
    }

    public  static List<List<Property>> classfyByDirection(List<Property> properties, Coordinate myLocation) {
        List<List<Property>> directions = new ArrayList<>();
        for (int i = 0; i< 8; ++i) {
            List<Property> direction = new ArrayList<>();
            directions.add(direction);
        }
        properties.forEach( property -> {
            double lat_differ = property.getCoordinate().getLatitude() - myLocation.getLatitude();
            double lon_differ = property.getCoordinate().getlongitude() - myLocation.getlongitude();
            int dir = getDirection(lat_differ, lon_differ);
            directions.get(dir).add(property);
        });
        directions.forEach( dir_properties -> {
            List<Property> temp = sortByDistanceAsc(dir_properties, myLocation);
            dir_properties = temp;
        });

        return directions;
    }

    private static int getDirection(double lat_differ, double lon_differ) {
        if (lat_differ > 0) {
            if (lon_differ > 0) {
                if (lat_differ - lon_differ > 0)
                    return 0;
                return 1;
            }
            else {
                if (lat_differ + lon_differ > 0)
                    return 2;
                return 3;
            }
        }
        else {
            if (lon_differ > 0) {
                if (lat_differ + lon_differ > 0)
                    return 4;
                return 5;
            }
            else {
                if (lat_differ - lon_differ > 0)
                    return 6;
                return 7;
            }
        }
    }

    public static String showMoney(int price) {
        return "$" + NumberFormat.getNumberInstance(Locale.US).format(price);
    }
}
