package com.example.josep.tdrealtor.task.old;

import android.os.AsyncTask;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

public abstract class ConvertUrlToISAsync extends AsyncTask<String, Void, Void> {

    private InputStream is;

    @Override
    protected Void doInBackground(String... urls) {
        try {
            is = (InputStream) new URL(urls[0]).getContent();
            return null;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        doTask();
    }

    protected InputStream getInputStream() { return is; }

    abstract protected void doTask();
}
