package com.example.josep.tdrealtor;

import android.content.Context;
import android.content.Intent;
import android.content.SearchRecentSuggestionsProvider;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class RegisterActivity extends AppCompatActivity {

    private CheckBox checkBoxInform, checkBoxEmail;
    private EditText editTextFirst, editTextLast, editTextPhone, editTextEmail;
    private Button buttonRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        checkBoxEmail = (CheckBox)findViewById(R.id.checkBoxAcceptEmail);
        checkBoxInform = (CheckBox)findViewById(R.id.checkBoxAcceptInform);

        editTextFirst = findViewById(R.id.editTextFirstName);
        editTextLast = findViewById(R.id.editTextLastName);
        editTextPhone = findViewById(R.id.editTextPhone);
        editTextEmail = findViewById(R.id.editTextEmail);
        buttonRegister = findViewById(R.id.buttonRegister);
        buttonRegister.setEnabled(false);
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences sharedPreferences = getSharedPreferences("tdRealtor",Context.MODE_PRIVATE);
        String firstname = sharedPreferences.getString("firstName", "");
        String lastname = sharedPreferences.getString("lastName", "");
        String email = sharedPreferences.getString("eMail", "");
        String phone = sharedPreferences.getString("phone", "");
        boolean acceptedRecieve = sharedPreferences.getBoolean("acceptedRecieve", false);
        boolean wasRegisterd = sharedPreferences.getBoolean("wasRegisterd",false);

        editTextFirst.setText(firstname);
        editTextLast.setText(lastname);
        editTextPhone.setText(phone);
        editTextEmail.setText(email);
        checkBoxInform.setChecked(wasRegisterd);
        checkBoxEmail.setChecked(acceptedRecieve);
    }

    public void onCheckboxInformClick (View view) {
        buttonRegister.setEnabled(checkBoxInform.isChecked());
    }

    public void onRegisterClick (View view) {

        if (editTextEmail.getText().toString().equals(""))  {
            Toast.makeText(this, "email address is required.", Toast.LENGTH_LONG).show();
        }
        if (!checkBoxInform.isChecked()) {
            Toast.makeText(this, "After you accept TD can use your information, you can use TDRealtor.", Toast.LENGTH_LONG).show();
        }
        else {
            saveSharedPreference();

            finish();
        }
    }

    public void saveSharedPreference() {
        SharedPreferences sharedPreferences = getSharedPreferences("tdRealtor",
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("firstName", editTextFirst.getText().toString());
        editor.putString("lastName", editTextLast.getText().toString());
        editor.putString("phone", editTextPhone.getText().toString());
        editor.putString("eMail",editTextEmail.getText().toString());
        editor.putBoolean("wasRegisterd", checkBoxInform.isChecked());
        editor.putBoolean("acceptedRecieve", checkBoxEmail.isChecked());
        editor.commit();
    }

    @Override
    protected void onPause() {
        super.onPause();
        SharedPreferences sharedPreferences = getSharedPreferences("tdRealtor",
                Context.MODE_PRIVATE);
       saveSharedPreference();
    }
}
