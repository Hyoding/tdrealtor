package com.example.josep.tdrealtor.session;

public class MySession extends Session {
    private static MySession instance = null;
    private MySession() {}

    public static MySession getInstance() {
        if (instance == null)
            instance = new MySession();

        return instance;
    }
}
