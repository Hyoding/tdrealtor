package com.example.josep.tdrealtor.property;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.josep.tdrealtor.location.Coordinate;

public class Property implements Parcelable {
    private int price;
    private Coordinate coordinate;
    private String address; // 2481 Norland Street, London, Ontario
    private String bedrooms;
    private String bathrooms;
    private String parkingSize;
    private String imageUrl;
    private String remarks; // from the landlord or realtor???
    private String sizeInterior; // 2500 squarefeet
    private String stories; // how tall
    private String type; // house, condo, etc?

    public Realtor getRealtor() {
        return realtor;
    }

    public void setRealtor(Realtor realtor) {
        this.realtor = realtor;
    }

    private Realtor realtor;
    private String detailsUrl;
    private String mlsNumber;

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    private Property(PropertyBuilder builder) {
        this.price = builder.price;
        this.coordinate = builder.coordinate;
        this.address = builder.address;
        this.bedrooms = builder.bedrooms;
        this.bathrooms = builder.bathrooms;
        this.parkingSize = builder.parkingSize;
        this.imageUrl = builder.imageUrl;
        this.remarks = builder.remarks;
        this.sizeInterior = builder.sizeInterior;
        this.stories = builder.stories;
        this.type = builder.type;
        this.realtor = builder.realtor;
        this.detailsUrl = builder.detailsUrl;
        this.mlsNumber = builder.mlsNumber;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public Coordinate getCoordinate() {
        return coordinate;
    }

    public void setCoordinate(Coordinate coordinate) {
        this.coordinate = coordinate;
    }

    public String getAddress() { return address; }

    public void setAddress(String address) { this.address = address; }

    public String getBedrooms() {
        return bedrooms;
    }

    public void setBedrooms(String bedrooms) {
        this.bedrooms = bedrooms;
    }

    public String getBathrooms() {
        return bathrooms;
    }

    public void setBathrooms(String bathrooms) {
        this.bathrooms = bathrooms;
    }

    public String getParkingSize() {
        return parkingSize;
    }

    public void setParkingSize(String parkingSize) {
        this.parkingSize = parkingSize;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getDetailsUrl() {
        return detailsUrl;
    }

    public void setDetailsUrl(String detailsUrl) {
        this.detailsUrl = detailsUrl;
    }

    public String getMlsNumber() {
        return mlsNumber;
    }

    public void setMlsNumber(String mlsNumber) {
        this.mlsNumber = mlsNumber;
    }

    public String getSizeInterior() {
        return sizeInterior;
    }

    public void setSizeInterior(String sizeInterior) {
        this.sizeInterior = sizeInterior;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(price);
        dest.writeParcelable(coordinate, flags);
        dest.writeString(address);
        dest.writeString(bedrooms);
        dest.writeString(bathrooms);
        dest.writeString(parkingSize);

        dest.writeString(imageUrl);
        dest.writeString(remarks);
        dest.writeString(sizeInterior);
        dest.writeString(stories);
        dest.writeString(type);
        dest.writeParcelable(realtor, flags);
        dest.writeString(detailsUrl);
        dest.writeString(mlsNumber);
    }

    private Property(Parcel in) {
        this.price = in.readInt();
        this.coordinate = in.readParcelable(Coordinate.class.getClassLoader());
        this.address = in.readString();
        this.bedrooms = in.readString();
        this.bathrooms = in.readString();
        this.parkingSize = in.readString();
        this.imageUrl = in.readString();
        this.remarks = in.readString();
        this.sizeInterior = in.readString();
        this.stories = in.readString();
        this.type = in.readString();
        this.realtor = in.readParcelable(Realtor.class.getClassLoader());
        this.detailsUrl = in.readString();
        this.mlsNumber = in.readString();
    }

    public static final Parcelable.Creator<Property> CREATOR = new Parcelable.Creator<Property>() {
        @Override
        public Property createFromParcel(Parcel source) {
            return new Property(source);
        }

        @Override
        public Property[] newArray(int size) {
            return new Property[size];
        }
    };

    public static class PropertyBuilder {
        private int price;
        private Coordinate coordinate;
        private String address;
        private String bedrooms;
        private String bathrooms;
        private String parkingSize;
        private String imageUrl;
        private String remarks; // from the landlord or realtor???
        private String sizeInterior; // 2500 squarefeet
        private String stories; // how tall
        private String type; // house, condo, etc?
        private Realtor realtor;
        private String detailsUrl;
        private String mlsNumber;

        public PropertyBuilder(int price, Coordinate coordinate) {
            this.price = price;
            this.coordinate = coordinate;
        }

        public PropertyBuilder stories(String stories) {
            this.stories = stories;
            return this;
        }

        public PropertyBuilder detailsUrl(String detailsUrl) {
            this.detailsUrl = detailsUrl;
            return this;
        }

        public PropertyBuilder mlsNumber(String mlsNumber) {
            this.mlsNumber = mlsNumber;
            return this;
        }

        public PropertyBuilder type(String type) {
            this.type = type;
            return this;
        }

        public PropertyBuilder realtor(Realtor realtor) {
            this.realtor = realtor;
            return this;
        }

        public PropertyBuilder remarks(String remarks) {
            this.remarks = remarks;
            return this;
        }

        public PropertyBuilder sizeInterior(String sizeInterior) {
            this.sizeInterior = sizeInterior;
            return this;
        }

        public PropertyBuilder imageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
            return this;
        }

        public PropertyBuilder address(String address) {
            this.address = address;
            return this;
        }

        public PropertyBuilder bedrooms(String bedrooms) {
            this.bedrooms = bedrooms;
            return this;
        }

        public PropertyBuilder bathrooms(String bathrooms) {
            this.bathrooms = bathrooms;
            return this;
        }

        public PropertyBuilder parkingSize(String parkingSize) {
            this.parkingSize = parkingSize;
            return this;
        }

        public Property build() {
            return new Property(this);
        }
    }
}
