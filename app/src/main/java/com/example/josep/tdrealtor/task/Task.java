package com.example.josep.tdrealtor.task;

public interface Task {
    void execute();
}
