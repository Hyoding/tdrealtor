package com.example.josep.tdrealtor.task.old;

import android.graphics.drawable.Drawable;
import android.widget.ImageView;

import java.io.IOException;

public class ConvertUrlToISImageViewAsync extends ConvertUrlToISAsync {
    ImageView imageView;

    public ConvertUrlToISImageViewAsync(ImageView imageView) {
        this.imageView = imageView;
    }

    @Override
    protected void doTask() {
        Drawable d = Drawable.createFromStream(getInputStream(), "src name");
        imageView.setImageDrawable(d);

        try {
            getInputStream().close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
