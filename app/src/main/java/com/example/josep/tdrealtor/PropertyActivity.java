package com.example.josep.tdrealtor;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.URLSpan;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.josep.tdrealtor.location.Coordinate;
import com.example.josep.tdrealtor.property.PropertyUtility;
import com.example.josep.tdrealtor.session.MySession;
import com.example.josep.tdrealtor.property.Property;
import com.example.josep.tdrealtor.task.RetrieveAndDisplayImageAsync;

public class PropertyActivity extends AppCompatActivity {
    public static final int NOTIFICATION_ID = 1;
    // The id and name of the channel.
    public static final String CHANNEL_ID = "channel_01";
    public static final String CHANNEL_NAME = "TDRealtor CHANNEL";
    private static String SHARE_MSG = "TDRealtor - Take a look at this property!";

    // components
    // property
    private ImageView imageView;
    private TextView addressTextView;
    private TextView priceTextView;
    private TextView bedroomTextView;
    private TextView mlsidTextView;
    private TextView bathroomTextView;
    private TextView descriptionTextView;
    private TextView sizeInteriorTextView;

    // realtor
    private ImageView realtorImageView;
    private TextView realtorNameTextView;
    private TextView realtorOrgTextView;


    private Property property;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setTitle("    TDRealtor");
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.td_logo);
        setContentView(R.layout.activity_property);

        property = getIntent().getParcelableExtra(Property.class.getSimpleName());
        initComponents();
        showPropertyDetails();
        createNotificationChannel();

        boolean mortNotification = getIntent().getBooleanExtra("mortgage", false);
        if (mortNotification) {
            showMortSpecialist();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void initComponents() {
        // property
        imageView           = findViewById(R.id.home_imageView);
        addressTextView     = findViewById(R.id.address_textView);
        priceTextView       = findViewById(R.id.price_textView);
        bedroomTextView     = findViewById(R.id.bedroom_textView);
        mlsidTextView     = findViewById(R.id.mlsid_textView);
        bathroomTextView    = findViewById(R.id.bathroom_textView);
        descriptionTextView  = findViewById(R.id.description_textView);
        sizeInteriorTextView = findViewById(R.id.sizeinterior_textView);

        // realtor
        realtorImageView = findViewById(R.id.realtor_imageView);
        realtorNameTextView = findViewById(R.id.realtorname_textView);
        realtorOrgTextView = findViewById(R.id.realtororg_textView);
    }

    private void showPropertyDetails() {
        // property
        new RetrieveAndDisplayImageAsync(imageView).execute(property.getImageUrl());
        addressTextView.setText(property.getAddress());
        priceTextView.setText(PropertyUtility.showMoney(property.getPrice()));
        mlsidTextView.setText("id: " + (property.getMlsNumber()));
        bedroomTextView.setText("Beds: " + (property.getBedrooms()));
        bathroomTextView.setText("Baths: " + (property.getBathrooms()));
        descriptionTextView.setText(property.getRemarks());
        sizeInteriorTextView.setText("Interior size: " + property.getSizeInterior());

        // realtor
        new RetrieveAndDisplayImageAsync(realtorImageView).execute(property.getRealtor().getPhotoURL());
        realtorNameTextView.setText(property.getRealtor().getName());
        realtorOrgTextView.setText(property.getRealtor().getOrganization());
    }

    public void onDirectionClicked(View view) {
        Uri uri = Uri.parse(googleUrlBuilder(MySession.getInstance().getCoordinate(), property.getCoordinate())); // missing 'http://' will cause crashed
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }

    private String googleUrlBuilder(Coordinate origin, Coordinate destination) {
        final String BASE_URL = "https://www.google.com/maps/dir/?api=1";
        String url = BASE_URL + "&origin=" + origin.getLatitude() + "," + origin.getlongitude() + "&destination=" + destination.getLatitude() + "," + destination.getlongitude();
        url += "&travelmode=driving";
        return url;
    }

    public void onEmailClicked(View view) {
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("message/rfc6068");

        emailIntent.putExtra(Intent.EXTRA_SUBJECT, SHARE_MSG);
        emailIntent.putExtra(Intent.EXTRA_TEXT, makeStringUrl(property.getDetailsUrl()));

        try {
            startActivity(Intent.createChooser(emailIntent, "Send mail..."));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(this,
                    "There is no email client installed.", Toast.LENGTH_SHORT).show();
        }
    }

    private SpannableStringBuilder makeStringUrl(String str) {
        SpannableStringBuilder builder = new SpannableStringBuilder();
        int start = builder.length();
        builder.append(str);
        int end = builder.length();

        builder.setSpan(new URLSpan(property.getDetailsUrl()), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return builder;
    }

    public void onSmsClicked(View view) {
        Uri uri = Uri.parse("smsto:");
        Intent it = new Intent(Intent.ACTION_SENDTO, uri);
        it.putExtra("sms_body", SHARE_MSG + "\n\n" + makeStringUrl(property.getDetailsUrl()));
        startActivity(it);
    }

    public void onRealtorClicked(View view) {
        Uri uri = Uri.parse("smsto:" + property.getRealtor().getPhone());
        Intent it = new Intent(Intent.ACTION_SENDTO, uri);
        String realtorFName = property.getRealtor().getName().split(" ")[0];
        it.putExtra("sms_body", "Hi " + realtorFName + "\n\nI would like to know more about below property. Please contact me to set up a viewing :)" + "\n\n" + property.getMlsNumber());
        startActivity(it);
    }

    public void onGetMortgageSpecialistClicked(View view) {

        // send notification
        Intent intent = new Intent(this, PropertyActivity.class);
        intent.putExtra(Property.class.getSimpleName(), property);
        intent.putExtra("mortgage", true);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID);
        builder.setSmallIcon(R.drawable.mort_specialist);
        builder.setContentIntent(pendingIntent);
        builder.setAutoCancel(true);
        builder.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.app_icon));
        builder.setContentTitle("Mortgage Specialist Found");
        builder.setContentText("Click to see details!");

        NotificationManager notificationManager = (NotificationManager) getSystemService(
                NOTIFICATION_SERVICE);

        ProgressDialog nDialog;
        nDialog = new ProgressDialog(this);
        nDialog.setMessage("Finding your mortgage specialist..");
        nDialog.setTitle("Please Wait");
        nDialog.setIndeterminate(false);
        nDialog.setCancelable(true);
        nDialog.show();

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                nDialog.dismiss();
                showMortSpecialist();
                notificationManager.notify(NOTIFICATION_ID, builder.build());
            }
        }, 5000); // 3000 milliseconds delay
    }

    private void showMortSpecialist() {
        ProgressDialog nDialog = new ProgressDialog(PropertyActivity.this);
        nDialog.setIndeterminateDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.mort_specialist));
        nDialog.setTitle("Found your mortgage specialist");
        nDialog.setMessage("Dave Scafe, TD Mortgage Specialist, will contact you in a few minutes!");
        nDialog.show();
    }

    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, CHANNEL_NAME, importance);
            channel.setDescription("INFO3097 Notifications");
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }
}
