package com.example.josep.tdrealtor.session;

import com.example.josep.tdrealtor.location.Coordinate;
import com.example.josep.tdrealtor.property.Property;

import java.util.List;

public class Session {
    private Coordinate coordinate;
    private double radius; // how many radius around coord to show
    private List<Property> properties;

    public Session() {
        coordinate = new Coordinate();

        radius = 1; // by default it will show this km around
    }

    public Coordinate getCoordinate() {
        return coordinate;
    }

    public void setCoordinate(Coordinate coordinate) {
        this.coordinate = coordinate;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public List<Property> getProperties() { return properties; }

    public void setProperties(List<Property> properties) { this.properties = properties; }
}
