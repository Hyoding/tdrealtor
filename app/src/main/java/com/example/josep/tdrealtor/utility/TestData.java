package com.example.josep.tdrealtor.utility;

import com.example.josep.tdrealtor.R;
import com.example.josep.tdrealtor.location.Coordinate;
import com.example.josep.tdrealtor.property.Property;

import java.util.ArrayList;
import java.util.List;

public class TestData {
    public static List<Property> getTestPropertyListing() {
        List<Property> propertyList = new ArrayList();
        propertyList.add( // around home
                new Property.PropertyBuilder(519000, new Coordinate(43.041275, -81.274602))
                        .address("#58 - 400 Skyline Avenue, London, Ontario")
                        .bedrooms("3")
                        .build()
        );
        propertyList.add( // around home
                new Property.PropertyBuilder(300000, new Coordinate(43.040452, -81.276061))
                        .address("#Some address in london ontario")
                        .build()
        );
        propertyList.add( // around fanshawe down town
                new Property.PropertyBuilder(410000, new Coordinate(42.983367, -81.251665))
                        .address("some address around fanshawe1")
                        .build()
        );
        propertyList.add( // around fanshawe down town
                new Property.PropertyBuilder(420000, new Coordinate(42.982264, -81.251202))
                        .address("some address around fanshawe2")
                        .build()
        );
        propertyList.add( // around fanshawe down town
                new Property.PropertyBuilder(620000, new Coordinate(42.983359, -81.247436))
                        .address("some address around fanshawe3")
                        .build()
        );
        return propertyList;
    }
}
