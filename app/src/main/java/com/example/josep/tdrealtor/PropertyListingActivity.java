package com.example.josep.tdrealtor;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.josep.tdrealtor.session.MySession;
import com.example.josep.tdrealtor.property.Property;
import com.example.josep.tdrealtor.task.RetrieveAndDisplayImageAsync;
import com.example.josep.tdrealtor.task.RetrieveRealtorDataAsync;
import com.example.josep.tdrealtor.property.PropertyUtility;

import java.util.List;

import static com.example.josep.tdrealtor.location.CoordinateUtility.distanceInKM;


public class PropertyListingActivity extends AppCompatActivity {
    // Render comp
    ListView listView;
    Spinner sortSpinner;
    ProgressDialog waitProgressDialog;


    private List<Property> propertyList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setTitle("    TDRealtor");
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.td_logo);
        setContentView(R.layout.activity_property_listing);

        if (MySession.getInstance().getProperties() == null) {
            startProgressDialog();
            fetchRealtorDataAsync();
        } else {
            displayPropertyList();
        }
    }

    private void startProgressDialog() {
        waitProgressDialog = new ProgressDialog(this);
        waitProgressDialog.setMessage("Loading..");
        waitProgressDialog.setIndeterminate(false);
        waitProgressDialog.setCancelable(true);
        waitProgressDialog.show();
    }

    private void fetchRealtorDataAsync() {
        new RetrieveRealtorDataAsync(() -> {
            displayPropertyList();
            waitProgressDialog.dismiss();
        } ).execute(MySession.getInstance().getCoordinate());
    }

    private void initSortSpinner() {
        sortSpinner = findViewById(R.id.sort_spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.filter_array, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        sortSpinner.setAdapter(adapter);
        sortSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0: // distance low to high
                        propertyList = PropertyUtility.sortByDistanceAsc(propertyList, MySession.getInstance().getCoordinate());
                        break;
                    case 1: // distance high to low
                        propertyList = PropertyUtility.sortByDistanceDesc(propertyList, MySession.getInstance().getCoordinate());
                        break;
                    case 2: // price low to high
                        propertyList = PropertyUtility.sortByPriceAsc(propertyList);
                        break;
                    case 3: // price high to low
                        propertyList = PropertyUtility.sortByPriceDesc(propertyList);
                        break;
                }
                initListView();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void displayPropertyList() {
        propertyList = MySession.getInstance().getProperties();
        propertyList = PropertyUtility.sortByDistanceAsc(propertyList, MySession.getInstance().getCoordinate());
        initSortSpinner();
        initListView();
    }


    private void initListView() {
        listView = findViewById(R.id.listView);
        CustomAdapter customAdapter = new CustomAdapter();
        listView.setAdapter(customAdapter);
    }

    // this class adapts a layout (customlayout used for property list) to listView
    class CustomAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return propertyList.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final int pos = position; // created this because onClick lambda function below required a final
            convertView = getLayoutInflater().inflate(R.layout.customlayout, null);

            ImageView imageView = convertView.findViewById(R.id.imageView);
            new RetrieveAndDisplayImageAsync(imageView).execute(propertyList.get(pos).getImageUrl());

            TextView textView_name = convertView.findViewById(R.id.textView_price);
            textView_name.setText(PropertyUtility.showMoney(propertyList.get(position).getPrice()));

            String distance = Integer.toString((int)(distanceInKM(MySession.getInstance().getCoordinate(), propertyList.get(position).getCoordinate()) * 100));
            TextView textView_description = convertView.findViewById(R.id.textView_distance);
            textView_description.setText(distance + "M");

            convertView.setOnClickListener(v -> {
                Intent intent = new Intent(PropertyListingActivity.this, PropertyActivity.class);
                intent.putExtra(Property.class.getSimpleName(), propertyList.get(pos));
                startActivity(intent);
            });
            return convertView;
        }
    }
}
