package com.example.josep.tdrealtor.property;

import android.os.Parcel;
import android.os.Parcelable;

public class Realtor implements Parcelable { // realtor
    private String name;
    private String organization;
    private String phone;
    private String email; // this is not revealed by the realtorca api
    private String photoURL;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhotoURL() {
        return photoURL;
    }

    public void setPhotoURL(String photoURL) {
        this.photoURL = photoURL;
    }

    public Realtor() {}
    private Realtor(RealtorBuilder builder) {
        this.name = builder.name;
        this.organization = builder.organization;
        this.phone = builder.phone;
        this.email = builder.email;
        this.photoURL = builder.photoURL;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(organization);
        dest.writeString(phone);
        dest.writeString(email);
        dest.writeString(photoURL);
    }

    public static final Parcelable.Creator<Realtor> CREATOR = new Parcelable.Creator<Realtor>() {
        @Override
        public Realtor createFromParcel(Parcel source) {
            return new Realtor(source);
        }

        @Override
        public Realtor[] newArray(int size) {
            return new Realtor[size];
        }
    };

    private Realtor(Parcel in) {
        this.name = in.readString();
        this.organization = in.readString();
        this.phone = in.readString();
        this.email = in.readString();
        this.photoURL = in.readString();
    }

    public static class RealtorBuilder {
        private String name;
        private String organization;
        private String phone;
        private String email; // this is not revealed by the realtorca api
        private String photoURL;

        public RealtorBuilder(String name, String phone) {
            this.name = name;
            this.phone = phone;
        }

        public RealtorBuilder organization(String organization) {
            this.organization = organization;
            return this;
        }

        public RealtorBuilder email(String email) {
            this.email = email;
            return this;
        }

        public RealtorBuilder photoURL(String photoURL) {
            this.photoURL = photoURL;
            return this;
        }

        public Realtor build() { return new Realtor(this); }
    }
}
