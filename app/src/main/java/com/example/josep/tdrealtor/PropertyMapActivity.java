package com.example.josep.tdrealtor;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarFinalValueListener;
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar;
import com.crystal.crystalrangeseekbar.widgets.CrystalSeekbar;
import com.example.josep.tdrealtor.location.Coordinate;
import com.example.josep.tdrealtor.property.Property;
import com.example.josep.tdrealtor.property.PropertyUtility;
import com.example.josep.tdrealtor.session.MySession;
import com.example.josep.tdrealtor.task.RetrieveRealtorDataAsync;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.stream.Collectors;

import static com.example.josep.tdrealtor.location.CoordinateUtility.distanceInKM;
import static com.example.josep.tdrealtor.property.PropertyUtility.classfyByDirection;
import static com.example.josep.tdrealtor.property.PropertyUtility.getPropertiesForAR;

public class PropertyMapActivity extends FragmentActivity implements OnMapReadyCallback,
        GoogleMap.OnMyLocationClickListener,
        GoogleMap.OnMyLocationButtonClickListener,
        GoogleMap.OnCameraIdleListener,
        GoogleMap.OnInfoWindowClickListener {

    private GoogleMap mMap;

    private Button filter;
    private EditText editTextMinPrice, editTextMaxPrice, editTextRange;
    private int maxPrice = 0, minPrice = 0, range = 0;
    private List<Property> properties;

    boolean markerClicked = false;

    // filter stuff
    CrystalRangeSeekbar priceRangeSeekbar;
    TextView minRangeTextView;
    TextView maxRangeTextView;

    CrystalSeekbar distanceSeekbar;
    TextView distanceTextView;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_property_map);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        initPriceRangeSeekbar();
        initDistanceSeekbar();
    }

    private void initPriceRangeSeekbar() {
        priceRangeSeekbar = findViewById(R.id.price_rangeSeekbar);
        minRangeTextView = findViewById(R.id.leftRangeTextView);
        maxRangeTextView = findViewById(R.id.rightRangeTextView);

        priceRangeSeekbar.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {
                minRangeTextView.setText(PropertyUtility.showMoney(minValue.intValue()));
                maxRangeTextView.setText(PropertyUtility.showMoney(maxValue.intValue()));
            }
        });

        priceRangeSeekbar.setOnRangeSeekbarFinalValueListener(new OnRangeSeekbarFinalValueListener() {
            @Override
            public void finalValue(Number minValue, Number maxValue) {
                refreshMap();
            }
        });
    }

    private void initDistanceSeekbar() {
        distanceSeekbar = findViewById(R.id.distance_rangeSeekbar);
        distanceTextView = findViewById(R.id.distanceFilterTextView);

        distanceSeekbar.setOnSeekbarChangeListener((changed) -> {
            distanceTextView.setText(changed.intValue() + " KM");
        });

        distanceSeekbar.setOnSeekbarFinalValueListener((changed) -> {
            MySession.getInstance().setRadius(changed.intValue());
            refreshMap();
        });
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng location = new LatLng(MySession.getInstance().getCoordinate().getLatitude(), MySession.getInstance().getCoordinate().getlongitude());
        mMap.moveCamera(CameraUpdateFactory.newLatLng(location));
        mMap.setMinZoomPreference(12);
        mMap.setMaxZoomPreference(18);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mMap.setMyLocationEnabled(true);
        mMap.setOnMyLocationButtonClickListener(this);
        mMap.setOnMyLocationClickListener(this);
        mMap.setOnCameraIdleListener(this);
        mMap.setOnInfoWindowClickListener(this);
        mMap.setOnMarkerClickListener( ( marker )-> {
            markerClicked = true;
            return false;
        });
    }

    private void displayMarkers() {
//        properties = MySession.getInstance().getProperties();
        filterProperties();
        mMap.clear();
        // add markers
        properties.forEach( property -> {
            LatLng loc = new LatLng(property.getCoordinate().getLatitude(), property.getCoordinate().getlongitude());
            MarkerOptions marker = new MarkerOptions()
                    .position(loc)
                    .title(property.getAddress())
                    .snippet(PropertyUtility.showMoney(property.getPrice()))
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));

            mMap.addMarker(marker);
        });
    }

    @Override
    public void onMyLocationClick(@NonNull Location location) {
        Toast.makeText(this, "Current location:\n" + location, Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onMyLocationButtonClick() {
        Toast.makeText(this, "MyLocation button clicked", Toast.LENGTH_SHORT).show();
        return false;
    }

    private void refreshMap() {
        CameraPosition cp = mMap.getCameraPosition();
        new RetrieveRealtorDataAsync(() -> {

            properties = MySession.getInstance().getProperties();
            displayMarkers();

        }).execute(new Coordinate(cp.target.latitude, cp.target.longitude));
        Toast.makeText(this, "Refreshing", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onCameraIdle() {
        if (markerClicked) {
            markerClicked = false;
        } else {
            refreshMap();
        }
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        // Find property
        properties = MySession.getInstance().getProperties();
        String selectedStreet = marker.getTitle();
        Property selectedProperty = properties.stream().filter(property -> property.getAddress().equals(selectedStreet)).findAny().orElse(null);

        Intent intent = new Intent(this, PropertyActivity.class);
        intent.putExtra(Property.class.getSimpleName(), selectedProperty);
        startActivity(intent);
    }

    public void onFilterClick(View view) {

        mMap.clear();
        displayMarkers();
    }

    private void filterProperties () {
        int minPrice = Integer.parseInt(minRangeTextView.getText().toString().substring(1).replace(",", ""));
        int maxPrice = Integer.parseInt(maxRangeTextView.getText().toString().substring(1).replace(",", ""));

        properties = properties.stream().filter(p -> p.getPrice() >= minPrice && p.getPrice() <= maxPrice).collect(Collectors.toList());
    }

    private void testFunction(Coordinate myLocation) {

        List<Property> tempProperty = getPropertiesForAR(3, properties, myLocation);
        properties = tempProperty;
    }

    public void listOnClicked(View view) {
        MySession.getInstance().setProperties(properties);
        Intent intent = new Intent(this, PropertyListingActivity.class);
        startActivity(intent);
    }

    public void onFilterLayoutClicked(View view) {
        LinearLayout b = findViewById(R.id.filter_layout);
        int isVisible = b.getVisibility() == View.VISIBLE ? LinearLayout.GONE : LinearLayout.VISIBLE;
        b.setVisibility(isVisible);
    }
}
