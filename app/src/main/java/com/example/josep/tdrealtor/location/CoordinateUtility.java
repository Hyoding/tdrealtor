package com.example.josep.tdrealtor.location;

import android.location.Location;

import com.example.josep.tdrealtor.location.Coordinate;

public class CoordinateUtility {
    public static Location convertCoordToLocation(Coordinate coordinate) {
        Location loc = new Location("");
        loc.setLatitude(coordinate.getLatitude());
        loc.setLongitude(coordinate.getlongitude());
        return loc;
    }

    public static float distanceInKM(Location loc1, Location loc2) {
        return loc1.distanceTo(loc2) / 1000;
    }

    public static float distanceInKM(Coordinate coord1, Coordinate coord2) {
        Location loc1 = convertCoordToLocation(coord1);
        Location loc2 = convertCoordToLocation(coord2);
        return distanceInKM(loc1, loc2);
    }

    final static double ONE_DEGREE =  111.111; // in KM
    final static double WEIRD_FACTOR = 1.235; // had to factor this to get the numbers right
    public static Coordinate calculateCoordinateMin(Coordinate origin, double radius) {
        final double OFFSET = radius / (ONE_DEGREE * WEIRD_FACTOR);

        final double LAT_MIN = origin.getLatitude() - OFFSET;
        final double LONG_MIN = origin.getlongitude() - OFFSET;
        return new Coordinate(LAT_MIN, LONG_MIN);
    }

    public static Coordinate calculateCoordinateMax(Coordinate origin, double radius) {
        final double OFFSET = radius / (ONE_DEGREE * WEIRD_FACTOR);

        final double LAT_MAX = origin.getLatitude() + OFFSET;
        final double LONG_MAX = origin.getlongitude() + OFFSET;
        return new Coordinate(LAT_MAX, LONG_MAX);
    }
}
