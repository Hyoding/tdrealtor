package com.example.josep.tdrealtor;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.josep.tdrealtor.location.Coordinate;
import com.example.josep.tdrealtor.session.MySession;

import java.util.List;


public class MainActivity extends AppCompatActivity {

    private MySession mySession;
    LocationManager locationManager;
    LocationListener locationListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setTitle("    TDRealtor");
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.td_logo);
        setContentView(R.layout.activity_main);

        SharedPreferences sharedPreferences = getSharedPreferences("tdRealtor",Context.MODE_PRIVATE);
        boolean wasRegisterd = sharedPreferences.getBoolean("wasRegisterd",false);
        if (!wasRegisterd) {
            Intent intent = new Intent(this, RegisterActivity.class);
            startActivity(intent);
        }
        // Permission check
        // TODO: Need to do a check for this as well later
        ActivityCompat.requestPermissions(this, new String[]{
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.SEND_SMS
        }, 1);

        if(isNetworkAvailable()) {
            mySession = MySession.getInstance();
            getCurrentLocation();
        } else {
            // TODO: need to make this alert? something else
            Toast.makeText(this, "Network is not available", Toast.LENGTH_LONG).show();
        }
    }

    private void getCurrentLocation() {
        Location loc = getLastKnownLocation();
        if (loc == null) {
            Toast.makeText(this, "Location wasn't available", Toast.LENGTH_LONG).show();
            return;
        }
        updateMyLocation(new Coordinate(loc.getLatitude(), loc.getLongitude()));
    }

    private Location getLastKnownLocation() {
        locationManager = (LocationManager)getApplicationContext().getSystemService(LOCATION_SERVICE);
        List<String> providers = locationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {
            Location l = locationManager.getLastKnownLocation(provider);
            if (l == null) {
                continue;
            }
            if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                // Found best last known location: %s", l);
                bestLocation = l;
            }
        }
        return bestLocation;
    }

    private void initLocationServices() {
        // Acquire a reference to the system Location Manager
        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        // Define a listener that responds to location updates
        locationListener = new LocationListener() {
            public void onLocationChanged(Location location) {
                updateMyLocation(new Coordinate(location.getLatitude(), location.getLongitude()));
            }

            public void onStatusChanged(String provider, int status, Bundle extras) {}
            public void onProviderEnabled(String provider) {}
            public void onProviderDisabled(String provider) {}
        };

        // Register the listener with the Location Manager to receive location updates
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
    }

    public void updateMyLocation(Coordinate coordinate) {
        this.mySession.getCoordinate().setLatitude(coordinate.getLatitude());
        this.mySession.getCoordinate().setlongitude(coordinate.getlongitude());
        Log.d("Location updated", "Longtitude:" + mySession.getCoordinate().getlongitude() + ", Latitude: " + mySession.getCoordinate().getLatitude());
    }

    public void onShowListClicked(View view) {
        Intent intent = new Intent(this, PropertyListingActivity.class);
        startActivity(intent);
    }

    public void onShowMapClicked(View view) {
        Intent intent = new Intent(this, PropertyMapActivity.class);
        startActivity(intent);
    }

    public void onARClicked(View view) {
        Intent intent = new Intent(this, ARActivity.class);
        startActivity(intent);
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }
}
