package com.example.josep.tdrealtor.task;

import android.os.AsyncTask;
import android.util.Log;

import com.example.josep.tdrealtor.location.Coordinate;
import com.example.josep.tdrealtor.property.Property;
import com.example.josep.tdrealtor.property.Realtor;
import com.example.josep.tdrealtor.session.MySession;
import com.example.josep.tdrealtor.location.CoordinateUtility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class RetrieveRealtorDataAsync extends AsyncTask<Coordinate, Void, String> {
    private static String REALTORCA_ENDPOINT = "https://api2.realtor.ca/Listing.svc/PropertySearch_Post";
    private static String REALTORCA_WEBSITE = "https://www.realtor.ca";

    Task task;

    public RetrieveRealtorDataAsync(Task task) {
        this.task = task;
    }

    private int convertPriceToInt(String str) {
        str = str.replace("$", "").replace(",", ""); // remove "$" and ","
        return Integer.parseInt(str);
    }

    protected void onPostExecute(String res) {
        JSONObject jsonObject;
        List<Property> properties = new ArrayList();

        try {
            jsonObject = new JSONObject(res);
            JSONArray propertiesJsonArray = jsonObject.getJSONArray("Results");

            if (propertiesJsonArray != null) {
                for(int i = 0; i < propertiesJsonArray.length(); ++i) {
                    Property property = convertToProperty(propertiesJsonArray.getJSONObject(i));
                    if (property != null) { properties.add(property); }
                }
            }
        } catch (JSONException e) {
            Log.e("Error at RetrieveRealtorDataAsync", e.toString());
        }

        MySession.getInstance().setProperties(properties);
        task.execute();
    }

    private String buildRealtorcaUrlParameters(Coordinate coord) {
        final String BASE_PARAM_URL = "CultureId=1&ApplicationId=1&TransactionTypeId=2&PropertySearchTypeId=1&RecordsPerPage=200&";

        Coordinate min = CoordinateUtility.calculateCoordinateMin(coord, MySession.getInstance().getRadius());
        Coordinate max = CoordinateUtility.calculateCoordinateMax(coord, MySession.getInstance().getRadius());

        StringBuilder sb = new StringBuilder();
        sb.append(BASE_PARAM_URL);
        sb.append("LongitudeMin=" + min.getlongitude() + "&");
        sb.append("LatitudeMin=" + min.getLatitude() + "&");
        sb.append("LongitudeMax=" + max.getlongitude() + "&");
        sb.append("LatitudeMax=" + max.getLatitude());
        return sb.toString();
    }

    @Override
    protected String doInBackground(Coordinate... coords) {
        Coordinate coord = coords[0];
        String urlParameters = buildRealtorcaUrlParameters(coord);

        byte[] postData = urlParameters.getBytes( StandardCharsets.UTF_8 );
        int postDataLength = postData.length;
        String request = REALTORCA_ENDPOINT;
        try {
            URL url = new URL( request );
            HttpURLConnection conn= (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setInstanceFollowRedirects(false);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            conn.setRequestProperty("charset", "utf-8");
            conn.setRequestProperty("Content-Length", Integer.toString(postDataLength ));
            conn.setUseCaches(false);
            try( DataOutputStream wr = new DataOutputStream( conn.getOutputStream())) {
                wr.write( postData );
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(conn.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                return response.toString();
            }
        } catch(Exception ex) {
            Log.e("realtor data", "Failed");
            return null;
        }
    }

    private Property convertToProperty(JSONObject propertyJO) {
        Property property;

        try {
            JSONObject buildingPropertyJO = propertyJO.getJSONObject("Building");

            JSONObject propPropertyJO = propertyJO.getJSONObject("Property");
            JSONObject addressPropPropertyJO = propPropertyJO.getJSONObject("Address");
            JSONArray parkingPropPropertyJO = propPropertyJO.isNull("Parking") ? null : propPropertyJO.getJSONArray("Parking");
            JSONArray photoPropPropertyJO = propPropertyJO.getJSONArray("Photo");

            int price = convertPriceToInt(propPropertyJO.getString("Price"));
            Coordinate coordinate = new Coordinate(Double.parseDouble(addressPropPropertyJO.getString("Latitude")), Double.parseDouble(addressPropPropertyJO.getString("Longitude")));
            String address = addressPropPropertyJO.getString("AddressText");
            String bedrooms = buildingPropertyJO.getString("Bedrooms");
            String bathrooms = buildingPropertyJO.getString("BathroomTotal");
            String parkingSize = parkingPropPropertyJO == null ? "N/A" :
                    parkingPropPropertyJO.getJSONObject(0).isNull("Spaces") ? "N/A" :
                            parkingPropPropertyJO.getJSONObject(0).getString("Spaces") ;
//            String parkingSize = propPropertyJO.getString("ParkingSpaceTotal");
            String imageUrl = photoPropPropertyJO.getJSONObject(0).getString("HighResPath");
            String remarks = propertyJO.getString("PublicRemarks");
            String sizeInterior = buildingPropertyJO.isNull("SizeInterior") ? "N/A" : buildingPropertyJO.getString("SizeInterior");
            String stories = buildingPropertyJO.isNull("StoriesTotal") ? "N/A" : buildingPropertyJO.getString("StoriesTotal");
            String type = buildingPropertyJO.getString("Type");

            JSONObject realtorPropertyJO = propertyJO.getJSONArray("Individual").getJSONObject(0); // get first realtor
            JSONObject orgRealtorPropertyJO = realtorPropertyJO.getJSONObject("Organization");
            JSONObject phoneRealtorPropertyJO = realtorPropertyJO.getJSONArray("Phones").getJSONObject(0); // get first phone

            String name = realtorPropertyJO.getString("Name");
            String organization = orgRealtorPropertyJO.getString("Name");
            String phone = phoneRealtorPropertyJO.getString("AreaCode") + "-" + phoneRealtorPropertyJO.getString("PhoneNumber");
//            String photoURL = realtorPropertyJO.isNull("Photo") ? "" : realtorPropertyJO.getString("Photo");
            String photoURL = realtorPropertyJO.isNull("Photo") ? "" : realtorPropertyJO.getString("Photo").replace("lowres", "highres");

            Realtor realtor = new Realtor.RealtorBuilder(name, phone)
                    .organization(organization)
                    .photoURL(photoURL)
                    .build();

            property = new Property.PropertyBuilder(price, coordinate)
                    .address(address)
                    .bedrooms(bedrooms)
                    .bathrooms(bathrooms)
                    .parkingSize(parkingSize)
                    .imageUrl(imageUrl)
                    .remarks(remarks)
                    .sizeInterior(sizeInterior)
                    .stories(stories)
                    .type(type)
                    .realtor(realtor)
                    .detailsUrl(REALTORCA_WEBSITE + propertyJO.getString("RelativeDetailsURL"))
                    .mlsNumber(propertyJO.getString("MlsNumber"))
                    .build();
        } catch (Exception ex) {
            Log.e("Error at forming obj from json", ex.toString());
            return null;
        }
        return property;
    }
}